import { createApp, h } from "vue";
import { createInertiaApp } from "@inertiajs/inertia-vue3";
import { InertiaProgress } from "@inertiajs/progress";

InertiaProgress.init();

createInertiaApp({
    resolve: async (name) => {
        // const comps = import.meta.glob("./Pages/**/*.vue");
        // const match = comps[`./Pages/${name}.vue`];
        const page = await import(`./Pages/Frontend/Component/${name}.vue`);
        return page;
    },
    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            .use(plugin)
            .mount(el);
    },
});
